$( document ).ready(function() {
  	$("#hambuger").click(function() {
  	    $(this).find('.nav-icon').toggleClass("open");
  	    $(".header-inner").slideToggle();
  	});
    $(function() {
        $('.img-auto').focusPoint()
        $('.img-auto').addClass('MissingWH').attr({
        'data-focus-x': 0,
        'data-focus-y': 0
        })
        $(".MissingWH img").fadeIn();
        $(".MissingWH").focusPoint();
    })
});
$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});

// Fix IE
var addClassltIE11 = function() {
  if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
    if (window.navigator.userAgent.indexOf("Windows NT 10.0") != -1 || window.navigator.userAgent.indexOf("Windows NT 6.3") != -1) {
      $('body').addClass('ltie11');
    } else if (window.navigator.userAgent.indexOf("Windows NT 6.1") != -1) {
      $('body').addClass('ltie7');
    }
  }
}

$(function() {
  addClassltIE11();
});

$(document).ready(function(){  
    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');
        $('ul.tabs li').removeClass('current');
        $('.tab_content').removeClass('current');
        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    })
})
jQuery(function($) {
    var t=$(".pagetop");
    t.hide(),t.click(function(){return $("body,html").animate({scrollTop:0},400),!1}),$(window).scroll(function(){$(this).scrollTop()>450?t.fadeIn():t.fadeOut()});
});