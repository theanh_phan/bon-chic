<footer id="footer" class="page-footer">
	<div class="container">
		<div class="footer-l">
			<div class="logo-footer">
				<a href="./"><img src="/common/img/logo_footer.svg" alt="BON-CHIC" class="svg"></a>
			</div>
			<p class="footer-info">Nittou Bld.1F, 6-6-66, Kyuhoji, Yao-shi, Osaka, 581-0072, Japan<br>
			TEL : 072-924-2131 / FAX : 072-924-7225<br>
			open 11:30～15:00(L.O.14:00) &amp; 17:30～22:00(L.O.21:00)<br>
			closed：Monday / Tuesday</p>
		</div>
		<div class="footer-r">
			<a href="#" class="footer-social"><img src="/common/img/icon-fb.png" alt="facebook"></a>
			<small class="copyright">©2019 BON-CHIC.</small>
		</div>
	</div>
</footer>
<!-- end footer -->
<span class="pagetop"><img src="/common/img/pagetop.png" alt="PAGE TOP"></span>