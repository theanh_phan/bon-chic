<header id="header" class="page-header">
	<div class="logo-sp">
		<a href="/"><img src="/common/img/logo_main.svg" alt="BON-CHIC" class="svg"></a>
	</div>
	<div class="header-inner">
		<nav class="gnav">
			<ul class="menu">
				<li><a href="/concept/">CONCEPT</a></li>
				<li><a href="/menu/">MENU</a></li>
				<li><a href="/about/">ABOUT/ACCESS</a></li>
				<li class="site-logo"><a href="/"><img src="/common/img/logo_main.svg" alt="BON-CHIC" class="svg"></a></li>
				<li><a href="/news/">NEWS</a></li>
				<li><a href="/contact/">CONTACT</a></li>
			</ul>
		</nav>
		<a href="#" class="header-social"><img src="/common/img/icon-fb.png" alt="facebook"></a>
	</div>
	<div id="hambuger">
		<div class="nav-icon">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
</header>
<!-- end header -->