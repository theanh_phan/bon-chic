window.addEventListener('DOMContentLoaded', function() {
  new Slide();
});

var Slide = (function(){
  function Slide(){
    var s = this;
    this.target = 'slider';
    this.step  = 0.0003;
    this.scale = 1;
    this.currentSlide = 0;
    this.opacity = 0;
    this.timer;
    this.timeout;
    this.img;
    this.eles = document.getElementById(this.target).querySelectorAll('.slide figure');
    Array.prototype.forEach.call(s.eles, function(el, k) {
      var li = document.createElement('li');
      li.innerText = k;
      if (k == s.currentSlide) {
        li.classList.add('active');
      }
    })
    this.func_transition = function(){
      s.img = s.eles[s.currentSlide];
      closest(s.img,'.slide').style.opacity = 1;
      Array.prototype.forEach.call(s.eles,function(el,k){
        if(k != s.currentSlide) {
          closest(el,'.slide').classList.remove('active');
        } else {
          closest(el,'.slide').classList.add('active');
        }
      })
      s.scale+=s.step;
      if(s.scale >= 1.15){
        s.currentSlide+=1;
        closest(s.img,'.slide').style.opacity = 0;
        s.scale = 1;
        if(s.currentSlide > s.eles.length-1) {
          s.currentSlide = 0;
        }
      }
      s.timer = window.requestAnimFrame(s.func_transition);
    }
    this.sizeWindow = function(){
      Array.prototype.forEach.call(s.eles,function(el,i){
        closest(el,'.slide').style.opacity = s.opacity;
        var img = new Image();
        img.onload = function(){
          // s.getRatio(img);
        }
        img.src = el.querySelector('img').getAttribute('src');
      });
    }
    window.addEventListener('resize',function(){
      //s.sizeWindow();
    })
    window.addEventListener('load',function(){
      s.sizeWindow();
    })
    this.sizeWindow();
    this.func_transition();
  }
  return Slide;
})()

$(window).on('load',function(){
	$('.main-slider_item--txt').addClass('bounceInLeft');
});